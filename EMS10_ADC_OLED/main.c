#include <msp430.h> 
#include <stdint.h>

int ADC_naar_temp(int adc_waarde)
{
    int temperatuur = 1000 * ((adc_waarde / 1023.0) * 1.5);
    return temperatuur;
}

/* Functieprototypen voor de meegeleverde "oled_lib" */

/* Deze functie configureert het display
 * via i2c en tekent alvast het kader.
 */
void initDisplay();

/* Deze functie past de weergeven
 * temperatuur aan.
 *
 * int temp: De te weergeven temperatuur.
 * Max is 999 min is -99. Hierbuiten weer-
 * geeft het "Err-"
 *
 * Voorbeeld:
 * setTemp(100); //stel 10.0 graden in
 */
void setTemp(int temp);

/* Deze functie past de bovenste titel aan.
 * char tekst[]: de te weergeven tekst
 *
 * Voorbeeld:
 * setTitle("Hallo"); //laat Hallo zien
 */
void setTitle(char tekst[]);

int main(void)
{
	WDTCTL = WDTPW | WDTHOLD;	// stop watchdog timer
	
	//stel klok in op 16MHz
    DCOCTL = 0;
    BCSCTL1 = CALBC1_16MHZ; // Set range
    DCOCTL = CALDCO_16MHZ;  // Set DCO step + modulation */

	initDisplay();
	setTitle("Opdracht7.1.13");

	uint8_t temperatuur=0;

	__enable_interrupt();

	ADC10CTL1 = INCH_5 | SHS_0 | ADC10DIV_7 | ADC10SSEL_0;
	ADC10CTL0 = SREF_1 | ADC10SHT_0 | ADC10SR | REFBURST | REFON | ADC10ON | ADC10IE | ENC;

	while(1)
	{
	    ADC10CTL0 |= ADC10SC;           // ADC10SC aanzetten, ADC conversie
	    __delay_cycles(4000000);
	}
}

#pragma vector = ADC10_VECTOR
__interrupt void ADC10_ISR(void)
{
    float temp;
    float temps[10];
    static int aantal = 0;
    int i = 0;
    int j = 0;
    float som = 0;
    float gem = 0;

    temp = ADC_naar_temp(ADC10MEM);
    for (i = (aantal - 1); i > 0; i--)
    {
        temps[i] = temps[i - 1];
    }
    temps[0] = temp;
    som = 0;
    for (j = 0;  j < aantal; j++)
    {
        som += temps[j];
    }
    gem = som / aantal;
    if (aantal < 10)
    {
        aantal++;
    }

    setTemp(gem);
    ADC10CTL0 &= ~(ADC10IFG);
}
